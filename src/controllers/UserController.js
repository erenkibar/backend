const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const { body, validationResult } = require('express-validator')

const UserService = require('../services/UserService')

function login(req, res) {

  const errors = validationResult(req)

  if (!errors.isEmpty())
    res.status(200).json({
      success: false,
      errors
    })
  else {
    UserService.authenticate(req.body)
      .then(token => {
        res.status(200).json({
          success: true,
          token: 'Bearer ' + token
        })
      })
      .catch(err => {
        res.status(200).json({
          success: false,
          message: 'Incorrect email or password'
        })
      })
  }
}

function register(req, res) {
  console.log(req)
  const errors = validationResult(req)

  if (!errors.isEmpty())
    res.status(200).json({
      success: false,
      errors
    })
  else {
    UserService.getUserByEmail(req.body.email)
    .then(exists => {
      if (exists) {
        return res.status(200).json({
          succes: false,
          message: 'E-mail already registered.'
        })
      }
      const newUser = {
        username: req.body.username,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 2)
      }
      return UserService.addUser(newUser)
        .then(() => res.status(200).json({
          success: true,
          message: 'Successfully registered!'
        }))
    })
  }
}

const validate = (method) => {
  switch (method) {
    case 'login': {
      return [
        body('email').exists().isEmail().withMessage('Invalid e-mail'),
        body('password').exists().notEmpty().withMessage('Invalid password')
      ]
    }
    case 'register': {
      return [
        body('username').exists().notEmpty().withMessage('Invalid username'),
        body('email').exists().isEmail().withMessage('Invalid e-mail'),
        body('password')
        .exists()
        .notEmpty()
        .withMessage('Invalid password'),
        body('password2')
        .exists()
        .custom((value, {req}) => value === req.body.password)
        .withMessage('Passwords do not match')
        .notEmpty()
        .withMessage('Invalid password'),

      ]
    }
  }
}

module.exports = {
  login,
  register,
  validate,
}