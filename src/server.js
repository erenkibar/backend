

// Patches
const { inject, errorHandler } = require('express-custom-error');
inject(); // Patch express in order to use async / await syntax

// Require Dependencies
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport')
const logger = require('./util/logger');

// Load .env Enviroment Variables to process.env
require('mandatoryenv').load([
    'DB_URL',
    'PORT',
    'SECRET'
]);

const { DB_URL } = process.env
const { PORT } = process.env || 5000;

// Instantiate an Express Application
const app = express();


// Configure Express App Instance
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: true, limit: '10mb' }));

// Configure custom logger middleware
app.use(logger.dev, logger.combined);

app.use(cookieParser());
app.use(cors());
app.use(helmet());

// This middleware adds the json header to every response
app.use('*', (req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    next();
})

// Bodyparser middleware
app.use(
    bodyParser.urlencoded({
        extended: false
    })
);
app.use(bodyParser.json());


//database connection
mongoose.connect(
    DB_URL,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
)
    .then(() => console.log('MongoDB successfully connected!'))
    .catch(err => console.log(err));

app.use(passport.initialize())
require('./config/passport')(passport)


// Assign Routes
app.use('/', require('./api/router.js'));
app.use('/users', require('./api/routes/users'))

// Handle errors
app.use(errorHandler());

// Handle not valid route
app.use('*', (req, res) => {
    res
        .status(404)
        .json({ status: false, message: 'Endpoint Not Found' });
})


// Open Server on selected Port
app.listen(
    PORT,
    () => console.info('Server listening on port ', PORT)
);