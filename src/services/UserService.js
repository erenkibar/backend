const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require('../models/User')


const authenticate = params => {
  const { SECRET } = process.env
  const { email, password } = params

  return User.findOne({ email }).then(user => {
    if (!user)
      throw new Error('Invalid email or password')
    if (!bcrypt.compare(password, user.password))
      throw new Error('Invalid email or password')
    const payload = {
      id: user.id,
      name: user.username
    }
    var token = jwt.sign(payload, SECRET, { expiresIn: 7200 })
    return token
  })
}

const addUser = user => User.create(user)

const getUserByEmail = email => User.findOne({ email })

module.exports = {
  authenticate,
  addUser,
  getUserByEmail
}
